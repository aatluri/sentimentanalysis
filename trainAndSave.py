import nltk
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
import pickle
from nltk.classify import ClassifierI
from statistics import mode
from nltk.tokenize import word_tokenize
import random

'''
Basically we take all the words in all the reviews. We then take the most popular of these words. We then group these 
words by which ones are associated with a postive category and which ones are associated with a negative category. Then 
when we want to classify a new review, we again get the most popular words from this new review and see whether most of 
them re in the positive or negative category and classify based on that. This is Naive Bayes algorithm.

TODO :
1. PreProcessing : 
    Removing HTML Markup
    Dealing with Punctuation, Numbers and Stopwords
    Remove names
2. Use Term Frequency to create feature sets
2. Include uni-grams and bi-grams when creating features and compare
3. Eliminate low information features
2. Add More Training Data
3. Investigate how to deal with bias

'''

short_neg = open("short_reviews/negative.txt", "r").read()
short_pos = open("short_reviews/positive.txt", "r").read()


all_words = []
documents = []

#  j is adject, r is adverb, and v is verb
# allowed_word_types = ["J","R","V"]
allowed_word_types = ["J"]

for p in short_pos.split('\n'):
    documents.append((p, "pos"))
    words = word_tokenize(p)
    pos = nltk.pos_tag(words)
    for w in pos:
        if w[1][0] in allowed_word_types:
            all_words.append(w[0].lower())

for p in short_neg.split('\n'):
    documents.append((p, "neg"))
    words = word_tokenize(p)
    pos = nltk.pos_tag(words)
    for w in pos:
        if w[1][0] in allowed_word_types:
            all_words.append(w[0].lower())


save_documents = open("pickled_algos/documents.pickle", "wb")
pickle.dump(documents, save_documents)
save_documents.close()


all_words = nltk.FreqDist(all_words)


word_features = list(all_words.keys())[:5000]

save_word_features = open("pickled_algos/word_features5k.pickle", "wb")
pickle.dump(word_features, save_word_features)
save_word_features.close()


def find_features(document):
    words = word_tokenize(document)
    features = {}
    for w in word_features:
        features[w] = (w in words)

    return features


featuresets = []

for (rev, category) in documents:
    featuresets.append((find_features(rev), category))

random.shuffle(featuresets)
print(len(featuresets))

training_set = featuresets[:10000]
testing_set = featuresets[10000:]

NaiveBayes_classifier = nltk.NaiveBayesClassifier.train(training_set)
print("Original Naive Bayes Algo accuracy percent:", (nltk.classify.accuracy(NaiveBayes_classifier, testing_set))*100)
NaiveBayes_classifier.show_most_informative_features(15)
NaiveBayes_classifier_save = open("pickled_algos/NaiveBayes5kFeatureSets.pickle", "wb")
pickle.dump(NaiveBayes_classifier, NaiveBayes_classifier_save)
NaiveBayes_classifier_save.close()

MNB_classifier = SklearnClassifier(MultinomialNB())
MNB_classifier.train(training_set)
MNB_classifier_save = open("pickled_algos/MNB5kFeatureSets.pickle", "wb")
pickle.dump(MNB_classifier, MNB_classifier_save)
MNB_classifier_save.close()
print("MNB_classifier accuracy percent:", (nltk.classify.accuracy(MNB_classifier, testing_set))*100)

BernoulliNB_classifier = SklearnClassifier(BernoulliNB())
BernoulliNB_classifier.train(training_set)
BernoulliNB_classifier_save = open("pickled_algos/BernoulliNB5kFeatureSets.pickle", "wb")
pickle.dump(BernoulliNB_classifier, BernoulliNB_classifier_save)
BernoulliNB_classifier_save.close()
print("BernoulliNB_classifier accuracy percent:", (nltk.classify.accuracy(BernoulliNB_classifier, testing_set))*100)

