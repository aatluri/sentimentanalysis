import nltk
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
import pickle
from nltk.classify import ClassifierI
from statistics import mode
from nltk.tokenize import word_tokenize
import random

'''
Basically we take all the words in all the reviews. We then take the most popular of these words. We then group these 
words by which ones are associated with a postive category and which ones are associated with a negative category. Then 
when we want to classify a new review, we again get the most popular words from this new review and see whether most of 
them re in the positive or negative category and classify based on that. This is Naive Bayes algorithm.

TODO :
1. PreProcessing : 
    Removing HTML Markup
    Dealing with Punctuation, Numbers and Stopwords
    Remove names
2. Use Term Frequency to create feature sets
2. Include uni-grams and bi-grams when creating features and compare
3. Eliminate low information features
2. Add More Training Data
3. Investigate how to deal with bias

'''


class VoteClassifier(ClassifierI):
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)
        return mode(votes)

    def confidence(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)

        choice_votes = votes.count(mode(votes))
        conf = choice_votes / len(votes)
        return conf


documents_f = open("pickled_algos/documents.pickle", "rb")
documents = pickle.load(documents_f)
documents_f.close()




word_features5k_f = open("pickled_algos/word_features5k.pickle", "rb")
word_features = pickle.load(word_features5k_f)
word_features5k_f.close()


def find_features(document):
    words = word_tokenize(document)
    features = {}
    for w in word_features:
        features[w] = (w in words)

    return features


featuresets_f = open("pickled_algos/featuresets.pickle", "rb")
featuresets = pickle.load(featuresets_f)
featuresets_f.close()

random.shuffle(featuresets)
print(len(featuresets))

testing_set = featuresets[10000:]
training_set = featuresets[:10000]

open_file = open("pickled_algos/NaiveBayes5kFeatureSets.pickle", "rb")
NaiveBayes_classifier = pickle.load(open_file)
open_file.close()


open_file = open("pickled_algos/MNB5kFeatureSets.pickle", "rb")
MNB_classifier = pickle.load(open_file)
open_file.close()


open_file = open("pickled_algos/BernoulliNB5kFeatureSets.pickle", "rb")
BernoulliNB_classifier = pickle.load(open_file)
open_file.close()


voted_classifier = VoteClassifier(NaiveBayes_classifier,
                                  MNB_classifier,
                                  BernoulliNB_classifier)


def sentiment(text):
    feats = find_features(text)
    return voted_classifier.classify(feats),voted_classifier.confidence(feats)
